#+LaTeX_CLASS: beamer
#+LaTeX_CLASS_OPTIONS: [presentation, smaller]
#+LaTeX_COMPILER: xelatex
#+TITLE: Towards a task-based coupled direct solver for sparse/dense FEM/BEM
#+TITLE: linear systems
#+SUBTITLE: SOLHARIS and HPC scalable ecosystem plenary meeting
#+DATE: March 31, 2023
#+LANGUAGE: en
#+OPTIONS: H:2 toc:nil author:nil email:nil ^:{}
#+INCLUDE: ./include/macros.org
#+LaTeX_HEADER: \usetikzlibrary{matrix, positioning, decorations.pathreplacing}
#+LaTeX_HEADER: \input{include/latex-header}
#+LaTeX_HEADER: \author{
#+LaTeX_HEADER:   Emmanuel Agullo, Alfredo Buttari, \underline{Marek Felšöci},
#+LaTeX_HEADER:   Guillaume Sylvand
#+LaTeX_HEADER: }
#+BEAMER_HEADER: \institute[Inria, IRIT]{
#+BEAMER_HEADER:   \textbf{Centre Inria de l'Université de Bordeaux} \\
#+BEAMER_HEADER:   \textbf{Institut de Recherche en Informatique de Toulouse}
#+BEAMER_HEADER: }
#+BEAMER_HEADER: \input{include/beamer-header}

* Introduction

** Industrial context
   
#+ATTR_LaTeX: :width .3\textwidth
[[./figures/airbus.svg]]

- study the propagation of sound waves emitted by an aircraft
  - reduce acoustic pollution, certify prototypes
- double discretization for numerical simulations
  - jet flow and part of ambient air
    - Finite Elements Method (FEM) cite:RaviartThomas1977,ern2013theory
      {{{rarr}}} volume domain {{{v}}}
  - boundary of ambient air
    - Boundary Elements Method (BEM) cite:bem,Wang2004413 {{{rarr}}} surface
      domain {{{s}}}

#+CAPTION: An acoustic wave (blue arrow) emitted by the aircraft's engine,
#+CAPTION: reflected on the wing and crossing the jet flow. Real-life case
#+CAPTION: (left, original picture by David Barrie) and a numerical model
#+CAPTION: example (right).
#+ATTR_LATEX: :height .3\textheight
[[./figures/airplane-mesh.png]]

** Problem
:PROPERTIES:
:BEAMER_OPT: fragile
:END:

*Coupled* cite:casenave-phd,casenave2014coupled FEM/BEM linear system:

*** Left column
:PROPERTIES:
:BEAMER_col: 0.7
:END:

#+BEGIN_EXPORT LaTeX
\begin{equation*}
\begin{tikzpicture}[
  baseline = (current  bounding  box.center),
  mystyle/.style = {
    matrix of math nodes,
    every node/.append style = {
      text width = #1, align = center, minimum height = 1ex
    },
    nodes in empty cells,
    left delimiter=[,
    right delimiter=],
  }
]
\matrix[mystyle = .6cm] (A)
{
  A_{vv} & A_{sv}^T \\
  A_{sv} & A_{ss} \\
};
\draw[line width = 3pt, green]
  ([xshift = -15pt, yshift = 2pt]A-1-1.north west) --
  ([xshift = -15pt, yshift = 2pt]A-1-1.south west);
\draw[line width = 3pt, red]
  ([xshift = -15pt, yshift = -2pt]A-2-1.north west) --
  ([xshift = -15pt, yshift = -2pt]A-2-1.south west);
\draw[line width = 3pt, green]
  ([xshift = -2pt, yshift = 8pt]A-1-1.north west) --
  ([xshift = -2pt, yshift = 8pt]A-1-1.north east);
\draw[line width = 3pt, red]
  ([xshift = 2pt, yshift = 6.5pt]A-1-2.north west) --
  ([xshift = 2pt, yshift = 6.5pt]A-1-2.north east);

\node at ([xshift = 20pt, yshift = -1.2pt]A.east) {$\times$};

\matrix[mystyle = .4cm, right = 40pt of A] (X)
{
  x_v \\
  x_s \\
};

\node at ([xshift = 17pt, yshift = -1.2pt]X.east) {$=$};

\matrix[mystyle = .4cm, right = 35pt of X] (B)
{
  b_v \\
  b_s \\
};
\end{tikzpicture}
\end{equation*}
#+END_EXPORT

- *symmetric* coefficient matrices:
  - {{{green(sparse)}}} parts
    - $A_{vv}$ (FEM {{{rarr}}} {{{v}}})
    - $A_{sv}$ ({{{s}}}/{{{v}}} interaction)
  - a {{{red(dense)}}} part
    - $A_{ss}$ (BEM {{{rarr}}} {{{s}}})
- *finer* model $\rightarrow$ *larger* system
- *direct* solution via Schur complement cite:zhang2006schur
  
*** Right column
:PROPERTIES:
:BEAMER_col: 0.3
:END:

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/mesh.png]]

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/system.svg]]

** Direct solution
:PROPERTIES:
:BEAMER_OPT: fragile
:END:

*** Schur complement
:PROPERTIES:
:BEAMER_env: block
:END:

- reduce the problem on boundaries {{{rarr}}} simplify the system

#+BEGIN_EXPORT LaTeX
\vspace{-2.8ex}
\begin{equation*}
\begin{tikzpicture}[
  baseline = (current  bounding  box.center),
  mystyle/.style = {
    matrix of math nodes,
    every node/.append style = {
      text width = #1, align = center, minimum height = 1ex
    },
    nodes in empty cells,
    left delimiter=[,
    right delimiter=],
  },
  rstyle/.style = {
    matrix of math nodes,
    every node/.append style = {
      text width = #1, align = center, minimum height = 1ex, font = \scriptsize
    },
    nodes in empty cells
  }
]
\matrix[rstyle = .4cm] (R)
{
  R_1 \\
  R_2 \\
};

\matrix[mystyle = .6cm, right = 20pt of R] (A)
{
  A_{vv} & A_{sv}^T \\
  A_{sv} & A_{ss} \\
};
\draw[line width = 3pt, green]
  ([xshift = -15pt, yshift = 2pt]A-1-1.north west) --
  ([xshift = -15pt, yshift = 2pt]A-1-1.south west);
\draw[line width = 3pt, red]
  ([xshift = -15pt, yshift = -2pt]A-2-1.north west) --
  ([xshift = -15pt, yshift = -2pt]A-2-1.south west);
\draw[line width = 3pt, green]
  ([xshift = -2pt, yshift = 8pt]A-1-1.north west) --
  ([xshift = -2pt, yshift = 8pt]A-1-1.north east);
\draw[line width = 3pt, red]
  ([xshift = 2pt, yshift = 6.5pt]A-1-2.north west) --
  ([xshift = 2pt, yshift = 6.5pt]A-1-2.north east);

\node at ([xshift = 20pt, yshift = -1.2pt]A.east) {$\times$};

\matrix[mystyle = .4cm, right = 40pt of A] (X)
{
  x_v \\
  x_s \\
};

\node at ([xshift = 17pt, yshift = -1.2pt]X.east) {$=$};

\matrix[mystyle = .4cm, right = 35pt of X] (B)
{
  b_v \\
  b_s \\
};
\end{tikzpicture}
\end{equation*}
\vspace{-1ex}
#+END_EXPORT

*** Computation steps
:PROPERTIES:
:BEAMER_env: block
:END:

1. *eliminate $x_v$ from the 2^{nd} equation {{{rarr}}} Schur complement $S$*
   #+BEGIN_EXPORT LaTeX
\vspace{-4ex}
\begin{equation*}
\hspace{-4ex}
\kbordermatrix{
  & & \\
  R_1 & A_{vv} & A_{sv}^T \\
  R_2 \leftarrow R_2 - A_{sv}A_{vv}^{-1} \times R_1 & 0 &
    S \leftarrow \boxed{A_{ss} - A_{sv}A_{vv}^{-1}A_{sv}^T}
}
\times
\begin{bmatrix}
  x_v \\
  x_s
\end{bmatrix}
=
\begin{bmatrix}
  b_v \\
  b_s - A_{sv}A_{vv}^{-1}b_v
\end{bmatrix}
\end{equation*}
\vspace{-3ex}
   #+END_EXPORT
2. solve the reduced Schur complement system
   #+BEGIN_EXPORT LaTeX
\vspace{-1.5ex}
\begin{equation*}
  Sx_s = b_s - A_{sv}A_{vv}^{-1}b_v
\end{equation*}
\vspace{-5.5ex}
   #+END_EXPORT
3. determine $x_v$ using $x_s$
   #+BEGIN_EXPORT LaTeX
\vspace{-1.5ex}
\begin{equation*}
  x_v = A_{vv}^{-1}(b_v - A_{sv}^Tx_s)
\end{equation*}
   #+END_EXPORT

** Numerical computation (1/2)

*** Properties of the input linear system
:PROPERTIES:
:BEAMER_env: block
:END:

**** Left column
:PROPERTIES:
:BEAMER_col: 0.75
:END:

- $A_{vv}$ and $A_{ss}$ are _symmetric_
  - storing only half of the coefficients
- $A_{vv}$ and $A_{sv}$ are _sparse_
  - storing only non-zero values

**** Right column
:PROPERTIES:
:BEAMER_col: 0.25
:END:

#+BEAMER: \vspace{-3ex}

#+CAPTION: Initial state of $A$
#+ATTR_LaTeX: :width 1\textwidth
[[./figures/system.svg]]

*** Ideal computation of $S = A_{ss} - A_{sv}A_{vv}^{-1}A_{vs}$
:PROPERTIES:
:BEAMER_env: block
:END:

**** Left column
:PROPERTIES:
:BEAMER_col: 0.75
:END:

- factorization of $A_{vv}$ into $L_{vv}L_{vv}^T \rightarrow$ _fill-in_
  #+BEGIN_EXPORT LaTeX
  \begin{equation*}
S = A_{ss} - A_{sv}(L_{vv}L_{vv}^T)^{-1}A_{sv}^T
  \end{equation*}
  #+END_EXPORT
- computation of the Schur complement
  #+BEGIN_EXPORT LaTeX
  \begin{equation*}
S = A_{ss} - \underbrace{(A_{sv}(L_{vv}^{T})^{-1})}_{\text{triangular solve}} \times
     \underbrace{(A_{sv}(L_{vv}^{T})^{-1})^T}_{\text{implicitly known}}
  \end{equation*}
  #+END_EXPORT

**** Right column
:PROPERTIES:
:BEAMER_col: 0.25
:END:

#+BEAMER: \vspace{-3ex}

#+CAPTION: $A$ after computing $S$
#+ATTR_LaTeX: :width 1\textwidth
[[./figures/system-fill-in.svg]]

** Numerical computation (2/2)

*** Implementation
:PROPERTIES:
:BEAMER_env: block
:END:

- combination of sparse and dense direct solver features
- existing building blocks of state-of-the-art solvers

*** Black-boxes
:PROPERTIES:
:BEAMER_env: ignoreheading
:END:

**** Compression
:PROPERTIES:
:BEAMER_col: 0.33
:END:

#+ATTR_LaTeX: :width .33\textwidth
[[./figures/black-box-compression.svg]]

**** Out-of-core
:PROPERTIES:
:BEAMER_col: 0.33
:END:

#+ATTR_LaTeX: :width .33\textwidth
[[./figures/black-box-ooc.svg]]

**** Distributed-memory parallelism
:PROPERTIES:
:BEAMER_col: 0.33
:END:

#+ATTR_LaTeX: :width .33\textwidth
[[./figures/black-box-distributed.svg]]

*** Vanilla solver couplings
:PROPERTIES:
:BEAMER_env: block
:END:

- straightforward application of building blocks to $A_{vv}$, $A_{sv}$ and
  $A_{ss}$
  - {{{coupling(b)}}} coupling
  - {{{coupling(a)}}} coupling

** {{{coupling(b)}}} coupling
   
*** Top part
:PROPERTIES:
:BEAMER_env: ignoreheading
:END:

**** Left column
:PROPERTIES:
:BEAMER_col: 0.5
:END:

- separate $A_{vv}$, $A_{sv}$ and $A_{ss}$
- {{{sfacto}}}, {{{ssolve}}}
- {{{dfacto}}}, {{{dsolve}}}

**** Right column
:PROPERTIES:
:BEAMER_col: 0.5
:END:

#+BEGIN_EXPORT LaTeX
\begin{center}
\only<1>{\includesvg[width = 1\textwidth]{./figures/baseline-initial-state}}
\only<2>{\includesvg[width = 1\textwidth]{./figures/baseline-sfacto}}
\only<3>{\includesvg[width = 1\textwidth]{./figures/baseline-ssolve}}
\only<4>{\includesvg[width = 1\textwidth]{./figures/baseline-intermediate}}
\only<5>{\includesvg[width = 1\textwidth]{./figures/baseline-schur}}
\only<6->{\includesvg[width = 1\textwidth]{./figures/baseline-dfacto}}
\end{center}
#+END_EXPORT

*** Bottom part
:PROPERTIES:
:BEAMER_env: ignoreheading
:END:

#+BEGIN_EXPORT LaTeX
\only<1>{
\begin{equation*}
\begin{bmatrix}
  A_{vv} & A_{sv}^T \\
  A_{sv} & A_{ss}
\end{bmatrix}
\times
\begin{bmatrix}
  x_v \\
  x_s
\end{bmatrix}
=
\begin{bmatrix}
  b_v \\
  b_s
\end{bmatrix}
\end{equation*}
}\only<2>{
\begin{equation*}
\begin{bmatrix}
  (L_{vv}L_{vv}^{T})^{-1} & A_{sv}^T \\
  A_{sv} & A_{ss}
\end{bmatrix}
\times
\begin{bmatrix}
  x_v \\
  x_s
\end{bmatrix}
=
\begin{bmatrix}
  b_v \\
  b_s
\end{bmatrix}
\end{equation*}
}\only<3>{
\begin{equation*}
\begin{bmatrix}
  (L_{vv}L_{vv}^{T})^{-1} & Y \leftarrow \boxed{(L_{vv}L_{vv}^{T})^{-1}A_{sv}^T} \\
  A_{sv} & A_{ss}
\end{bmatrix}
\times
\begin{bmatrix}
  x_v \\
  x_s
\end{bmatrix}
=
\begin{bmatrix}
  b_v \\
  b_s
\end{bmatrix}
\end{equation*}
}\only<4-5>{
\begin{equation*}
\begin{bmatrix}
  (L_{vv}L_{vv}^{T})^{-1} & Y \leftarrow \boxed{(L_{vv}L_{vv}^{T})^{-1}A_{sv}^T} \\
  A_{sv} & S \leftarrow \boxed{A_{ss} - A_{sv}Y}
\end{bmatrix}
\times
\begin{bmatrix}
  x_v \\
  x_s
\end{bmatrix}
=
\begin{bmatrix}
  b_v \\
  b_s
\end{bmatrix}
\end{equation*}
}\only<6>{
\begin{equation*}
\begin{bmatrix}
  (L_{vv}L_{vv}^{T})^{-1} & Y \leftarrow \boxed{(L_{vv}L_{vv}^{T})^{-1}A_{sv}^T} \\
  A_{sv} & (L_{S}L_{S}^{T})^{-1}
\end{bmatrix}
\times
\begin{bmatrix}
  x_v \\
  x_s
\end{bmatrix}
=
\begin{bmatrix}
  b_v \\
  b_s
\end{bmatrix}
\end{equation*}
}\only<7>{
\begin{equation*}
\begin{bmatrix}
  (L_{vv}L_{vv}^{T})^{-1} & Y \leftarrow \boxed{(L_{vv}L_{vv}^{T})^{-1}A_{sv}^T} \\
  A_{sv} & (L_{S}L_{S}^{T})^{-1}
\end{bmatrix}
\times
\begin{bmatrix}
  x_v \\
  x_s \leftarrow \boxed{b_s - A_{sv}(L_{vv}L_{vv}^{T})^{-1}b_v}
\end{bmatrix}
=
\begin{bmatrix}
  b_v \\
  b_s
\end{bmatrix}
\end{equation*}
}\only<8->{
\begin{equation*}
\begin{bmatrix}
  (L_{vv}L_{vv}^{T})^{-1} & Y \leftarrow \boxed{(L_{vv}L_{vv}^{T})^{-1}A_{sv}^T} \\
  A_{sv} & (L_{S}L_{S}^{T})^{-1}
\end{bmatrix}
\times
\begin{bmatrix}
  x_v \leftarrow \boxed{(L_{vv}L_{vv}^{T})^{-1}(b_v - A_{sv}^Tx_s)}\\
  x_s \leftarrow \boxed{b_s - A_{sv}(L_{vv}L_{vv}^{T})^{-1}b_v}
\end{bmatrix}
=
\begin{bmatrix}
  b_v \\
  b_s
\end{bmatrix}
\end{equation*}
}
#+END_EXPORT

** {{{coupling(a)}}} coupling

*** Top part
:PROPERTIES:
:BEAMER_env: ignoreheading
:END:

**** Left column
:PROPERTIES:
:BEAMER_col: 0.5
:END:

- $A$ as a whole
- {{{facto_schur}}}
  - MUMPS cite:MUMPSguide
  - PaStiX cite:PaStiXGuide
  - PARDISO cite:PARDISOguide
- {{{dfacto}}}, {{{dsolve}}}

**** Right column
:PROPERTIES:
:BEAMER_col: 0.5
:END:

#+BEGIN_EXPORT LaTeX
\begin{center}
\only<1>{\includesvg[width = 1\textwidth]{./figures/advanced-initial-state}}
\only<2>{\includesvg[width = 1\textwidth]{./figures/advanced-intermediate}}
\only<3>{\includesvg[width = 1\textwidth]{./figures/advanced-schur}}
\only<4->{\includesvg[width = 1\textwidth]{./figures/advanced-dfacto}}
\end{center}
#+END_EXPORT

*** Bottom part
:PROPERTIES:
:BEAMER_env: ignoreheading
:END:

#+BEGIN_EXPORT LaTeX
\only<1>{
\begin{equation*}
\begin{bmatrix}
  A_{vv} & A_{sv}^T \\
  A_{sv} & A_{ss}
\end{bmatrix}
\times
\begin{bmatrix}
  x_v \\
  x_s
\end{bmatrix}
=
\begin{bmatrix}
  b_v \\
  b_s
\end{bmatrix}
\end{equation*}
}\only<2-3>{
\begin{equation*}
\begin{bmatrix}
  (L_{vv}L_{vv}^{T})^{-1} & A_{sv}^T \\
  A_{sv}(L_{vv}^{T})^{-1} & S \leftarrow A_{ss} + \boxed{
    - (A_{sv}(L_{vv}^{T})^{-1})(A_{sv}(L_{vv}^{T})^{-1})^T
  }
\end{bmatrix}
\times
\begin{bmatrix}
  x_v \\
  x_s
\end{bmatrix}
=
\begin{bmatrix}
  b_v \\
  b_s
\end{bmatrix}
\end{equation*}
}\only<4>{
\begin{equation*}
\begin{bmatrix}
  (L_{vv}L_{vv}^{T})^{-1} & A_{sv}^T \\
  A_{sv}(L_{vv}^{T})^{-1} & (L_{S}L_{S}^{T})^{-1}
\end{bmatrix}
\times
\begin{bmatrix}
  x_v \\
  x_s
\end{bmatrix}
=
\begin{bmatrix}
  b_v \\
  b_s
\end{bmatrix}
\end{equation*}
}\only<5->{
\begin{equation*}
\begin{bmatrix}
  (L_{vv}L_{vv}^{T})^{-1} & A_{sv}^T \\
  A_{sv}(L_{vv}^{T})^{-1} & (L_{S}L_{S}^{T})^{-1}
\end{bmatrix}
\times
\begin{bmatrix}
  x_v \leftarrow \boxed{(L_{vv}L_{vv}^{T})^{-1}(b_v - A_{sv}^Tx_s)} \\
  x_s \leftarrow \boxed{b_s - A_{sv}(L_{vv}L_{vv}^{T})^{-1}b_v}
\end{bmatrix}
=
\begin{bmatrix}
  b_v \\
  b_s
\end{bmatrix}
\end{equation*}
}
#+END_EXPORT

** Limitations of vanilla couplings in our context

*** Top part
:PROPERTIES:
:BEAMER_env: ignoreheading
:END:
   
**** {{{coupling(b)}}} coupling
:PROPERTIES:
:BEAMER_env: block
:BEAMER_col: 0.5
:END:

#+LaTeX: \only<1>{

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/baseline-schur.svg]]

#+LaTeX: }\only<2->{

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/baseline-schur-compressed.svg]]

#+LaTeX: }

**** {{{coupling(a)}}} coupling
:PROPERTIES:
:BEAMER_env: block
:BEAMER_col: 0.5
:END:

#+LaTeX: \only<1>{

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/advanced-schur.svg]]

#+LaTeX: }\only<2->{

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/advanced-schur-compressed.svg]]

#+LaTeX: }

*** Bottom part
:PROPERTIES:
:BEAMER_env: ignoreheading
:END:

**** Limitations (1)
:PROPERTIES:
:BEAMER_col: 0.45
:END:

- large dense non-compressed submatrices in RAM
  - *$Y$* and *Schur comp.*

**** Black-box
:PROPERTIES:
:BEAMER_col: 0.1
:END:

#+LaTeX: \only<2->{

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/black-box-compression.svg]]

#+LaTeX: }

**** Limitations (2)
:PROPERTIES:
:BEAMER_col: 0.45
:END:

- large dense non-compressed submatrix in RAM
  - *Schur comp.*

* Two-stage schemes

** From vanilla couplings to alternative schemes

*** Top part
:PROPERTIES:
:BEAMER_env: ignoreheading
:END:
   
#+BEGIN_EXPORT LaTeX
\begin{columns}
\begin{column}{.5\columnwidth}
\begin{block}{
  \only<1-2>{
    \textit{baseline} coupling
  }\only<3->{
    \textcolor{multisolve}{multi-solve}
  }
}
#+END_EXPORT

#+LaTeX: \only<1>{

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/baseline-schur-block-wise.svg]]

#+LaTeX: }\only<2>{

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/baseline-schur-block-wise-compressed.svg]]

#+LaTeX: }\only<3->{

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/bms-scaled.svg]]

#+LaTeX: }

#+BEGIN_EXPORT LaTeX
\end{block}
\end{column}
\begin{column}{.5\columnwidth}
\begin{block}{
  \only<1-3>{
    \textit{advanced} coupling
  }\only<4->{
    \textcolor{multifacto}{multi-factorization}
  }
}
#+END_EXPORT

#+LaTeX: \only<1>{

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/advanced-schur-block-wise.svg]]

#+LaTeX: }\only<2-3>{

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/advanced-schur-block-wise-compressed.svg]]

#+LaTeX: }\only<4>{

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/bmf-scaled.svg]]

#+LaTeX: }

#+BEGIN_EXPORT LaTeX
\end{block}
\end{column}
\end{columns}
#+END_EXPORT

*** Bottom part
:PROPERTIES:
:BEAMER_env: ignoreheading
:END:

**** Principle (1)
:PROPERTIES:
:BEAMER_col: 0.45
:END:

#+LaTeX: \only<1-2>{

- large dense non-compressed submatrices in RAM
  - *$Y$* and *Schur comp.*

#+LaTeX: }\only<3->{

#+BEGIN_EXPORT LaTeX
\vspace{-3em}
{\small
\begin{equation*}
S_i = A_{{ss}_i} - A_{sv}\overbrace{
  \textcolor{green}{(L_{vv}L_{vv}^T)^{-1}}\textcolor{orange}{A^T_{{sv}_i}}
}^{solve\ \rightarrow\ \textcolor{orange}{Y_i}}
\end{equation*}}
\vspace{-2em}
#+END_EXPORT

- 1 {{{sfacto}}} of the {{{green(green)}}} matrix (symm.)
- lot of {{{ssolve}}} on the {{{density(orange)}}} blocks (dense res.)

#+LaTeX: \vspace{-1em}

#+LaTeX: }

**** Black-box
:PROPERTIES:
:BEAMER_col: 0.1
:END:

#+LaTeX: \only<2>{

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/black-box-compression.svg]]

#+LaTeX: }

**** Principle (2)
:PROPERTIES:
:BEAMER_col: 0.45
:END:

#+LaTeX: \only<1-3>{

- large dense non-compressed submatrix in RAM
  - *Schur comp.*

#+LaTeX: }\only<4->{

#+BEGIN_EXPORT LaTeX
\vspace{-3em}
{\small
\begin{equation*}
S_{ij} = A_{{ss}_{ij}} - \overbrace{
  \textcolor{repetition}{
    A_{{sv}_i}(L_{vv}U_{vv})^{-1}A^T_{{sv}_j}
  }
}^{sparse\ facto.+Schur}
\end{equation*}}
\vspace{-2em}
#+END_EXPORT

- multiple {{{facto_schur}}} of the {{{repetition(violet)}}} matrix (non-symm.)

#+LaTeX: }

** Two-stage schemes

*** Text
:PROPERTIES:
:BEAMER_col: 0.9
:END:

**** Two stages
:PROPERTIES:
:BEAMER_env: block
:END:

1. block-wise assembly of the Schur complement $S$
2. factorization of $S$ and solve of $x_s$ and $x_v$

**** Principle
:PROPERTIES:
:BEAMER_env: block
:END:

- keep using fully-featured industrial-quality solvers
  - MUMPS (sparse)
  - SPIDO, HMAT (dense)
- API not designed to do it in one call {{{rarr}}} multiple calls

*** Black-boxes
:PROPERTIES:
:BEAMER_col: 0.1
:END:

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/black-box-compression.svg]]

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/black-box-ooc.svg]]

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/black-box-distributed.svg]]

** Experiments on PlaFRIM cite:plafrim and TGCC cite:tgcc

*** Schemes
:PROPERTIES:
:BEAMER_col: 0.2
:END:

#+CAPTION: {{{multisolve}}}
#+ATTR_LaTeX: :width 1\textwidth
[[./figures/bms.svg]]

#+LaTeX: \vspace{-1.5em}

#+CAPTION: {{{multifacto}}}
#+ATTR_LaTeX: :width 1\textwidth
[[./figures/bmf.svg]]

*** Results
:PROPERTIES:
:BEAMER_col: 0.6
:END:

**** Black-boxes
:PROPERTIES:
:BEAMER_env: ignoreheading
:END:

***** Compression
:PROPERTIES:
:BEAMER_col: 0.33
:END:

#+ATTR_LaTeX: :width .5\textwidth
[[./figures/black-box-compression.svg]]

***** Out-of-core
:PROPERTIES:
:BEAMER_col: 0.33
:END:

#+ATTR_LaTeX: :width .5\textwidth
[[./figures/black-box-ooc.svg]]

***** Distributed-memory parallelism
:PROPERTIES:
:BEAMER_col: 0.33
:END:

#+ATTR_LaTeX: :width .5\textwidth
[[./figures/black-box-distributed.svg]]

**** On 1 node (academic, symmetric)
:PROPERTIES:
:BEAMER_env: block
:END:

| vanilla {{{coupling(a)}}} coupling | 2M    |
| {{{multisolve}}}                   | _14M_ |
| {{{multifacto}}}                   | 4M    |

**** On 16 nodes (academic, symmetric)
:PROPERTIES:
:BEAMER_env: block
:END:

| vanilla {{{coupling(a)}}} coupling | 6M    |
| {{{multisolve}}}                   | _42M_ |
| {{{multifacto}}}                   | 7M    |

*** Test cases
:PROPERTIES:
:BEAMER_col: 0.2
:END:

#+CAPTION: Academic test case.
#+ATTR_LaTeX: :width 1\textwidth
[[./figures/pipe-2.png]]

** Experiments on HPC5 at Airbus

*** Schemes
:PROPERTIES:
:BEAMER_col: 0.2
:END:

#+CAPTION: {{{multisolve}}}
#+ATTR_LaTeX: :width 1\textwidth
[[./figures/bms.svg]]

#+LaTeX: \vspace{-1.5em}

#+CAPTION: {{{multifacto}}}
#+ATTR_LaTeX: :width 1\textwidth
[[./figures/bmf.svg]]

*** Results
:PROPERTIES:
:BEAMER_col: 0.6
:END:

**** Black-boxes
:PROPERTIES:
:BEAMER_env: ignoreheading
:END:

***** Compression
:PROPERTIES:
:BEAMER_col: 0.33
:END:

#+ATTR_LaTeX: :width .5\textwidth
[[./figures/black-box-compression.svg]]

***** Out-of-core
:PROPERTIES:
:BEAMER_col: 0.33
:END:

#+ATTR_LaTeX: :width .5\textwidth
[[./figures/black-box-ooc.svg]]

***** Distributed-memory parallelism
:PROPERTIES:
:BEAMER_col: 0.33
:END:

#+ATTR_LaTeX: :width .5\textwidth
[[./figures/black-box-distributed.svg]]

**** On 1 node (industrial, non-symm.)
:PROPERTIES:
:BEAMER_env: block
:END:

- vanilla {{{coupling(a)}}} coupling
  - could not run (out of RAM)
- {{{multisolve}}}
  - the least RAM consuming
- {{{multifacto}}}
  - the fastest

**** On multiple nodes (industrial)
:PROPERTIES:
:BEAMER_env: block
:END:

- {{{box(red, white, TODO)}}}

*** Test cases
:PROPERTIES:
:BEAMER_col: 0.2
:END:

#+CAPTION: Industrial test case.
#+ATTR_LaTeX: :width 1\textwidth
[[./figures/industrial.png]]

#+CAPTION: Simulation result.
#+ATTR_LaTeX: :width 1\textwidth
[[./figures/industrial-result.png]]

* Single-stage scheme

** Towards ideal implementation

*** Schemes
:PROPERTIES:
:BEAMER_col: 0.25
:END:

#+CAPTION: Ideal approach
#+ATTR_LaTeX: :width .65\textwidth
[[./figures/sparse-aware.svg]]

#+LaTeX: \vspace{-1.5em}

#+CAPTION: {{{multisolve}}}
#+ATTR_LaTeX: :width .8\textwidth
[[./figures/bms.svg]]

#+LaTeX: \vspace{-1.5em}

#+CAPTION: {{{multifacto}}}
#+ATTR_LaTeX: :width .8\textwidth
[[./figures/bmf.svg]]
   
*** Text
:PROPERTIES:
:BEAMER_col: 0.75
:END:

**** Ideal implementation
:PROPERTIES:
:BEAMER_env: block
:END:

- preserve and exploit symmetry and sparsity

**** Limitations of two-stage approaches
:PROPERTIES:
:BEAMER_env: block
:END:

- {{{multisolve}}}
  - storage of {{{density(orange)}}} blocks in a dense matrix
  - extra computations on null columns in $A_{sv}$
- {{{multifacto}}}
  - re-factorizations of {{{green(sparse)}}} $A_{vv}$
  - loss of symmetry
- synchronous calls to sparse and dense solvers

** Single-stage scheme

*** Left column
:PROPERTIES:
:BEAMER_col: 0.7
:END:

**** Prototype (with A. Buttari @ IRIT/ENSEEIHT)
:PROPERTIES:
:BEAMER_env: block
:END:

- coupling of task based direct solvers
  - {{{green(sparse)}}}: {{{qrm}}}
    - no compression, no distributed-memory parallelism (ongoing Ph.D.)
  - {{{red(dense)}}}: HMAT
  - relying on the StarPU runtime cite:Starpu2010
    - built-in out-of-core capability
- tile-wise assembly of $S$

**** Goals
:PROPERTIES:
:BEAMER_env: block
:END:

1. *ideal symmetry and sparsity*
2. asynchronous sparse/dense solver calls
3. out-of-core $S$

*** Right column
:PROPERTIES:
:BEAMER_col: 0.3
:END:

#+LaTeX: \only<1>{

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/sparse-aware.svg]]

#+LaTeX: }\only<2>{

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/ideal-initial-state.svg]]

#+LaTeX: }\only<3>{

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/ideal-sfacto.svg]]

#+LaTeX: }\only<4>{

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/ideal-schur.svg]]

#+LaTeX: }\only<5->{

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/ideal-dfacto.svg]]

#+LaTeX: }\only<6->{

#+LaTeX: \vspace{1em}

#+ATTR_LaTeX: :width .4\textwidth
[[./figures/black-box.svg]]

#+LaTeX: }

** Implementation

1. integrate {{{qrm}}} into the Airbus solver stack
2. implement {{{multisolve}}} for {{{qrm_spido}}}
   - for matrices with real coefficients
3. implement $LL^T$ for complex symmetric matrices [fn:llt] in {{{qrm}}}
4. implement {{{facto_schur}}} building block in {{{qrm}}}
5. implement {{{multifacto}}} for {{{qrm_spido}}}
6. extend the API of HMAT
7. in-core asynchronous single-stage prototype
   - based on {{{qrm_hmat}}}

[fn:llt] based on an existing $LL^T$ routine for real matrices


** Preliminary experimental evaluation on PlaFRIM cite:plafrim

#+BEGIN_EXPORT LaTeX
\begin{center}\Large
\only<1-2>{Does it work?}\only<3->{And asynchronous execution?}
\only<2>{Yes, it does!}\only<4>{Yes!}
\end{center}
#+END_EXPORT

*** Left column
:PROPERTIES:
:BEAMER_col: 0.5
:END:

#+LaTeX: \only<2-3>{

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/1stage-time-1.svg]]

#+LaTeX: }\only<4->{

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/1stage-time-2.svg]]

#+LaTeX: }

*** Right column
:PROPERTIES:
:BEAMER_col: 0.5
:END:

#+LaTeX: \only<2-3>{

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/1stage-rss-1.svg]]

#+LaTeX: }\only<4->{

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/1stage-rss-2.svg]]

#+LaTeX: }

** Preliminary comparison to other approaches

*** Left column
:PROPERTIES:
:BEAMER_col: 0.5
:END:

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/1stage-others-time.svg]]

*** Right column
:PROPERTIES:
:BEAMER_col: 0.5
:END:

#+ATTR_LaTeX: :width 1\textwidth
[[./figures/1stage-others-rss.svg]]

* Conclusion

** Conclusion

*** Two-stage algorithms cite:RR2020,compas2021,IPDPS22,SBACPAD22
:PROPERTIES:
:BEAMER_env: block
:END:

- keep using fully-featured solvers
- process larger systems compared to vanilla couplings
  - up to 9M vs. 2M in shared memory
  - up to 42M vs. 6M in distributed memory
  - industrial case impossible to run before
- remain suboptimal

*** Single-stage algorithm
:PROPERTIES:
:BEAMER_env: block
:END:

- non fully-featured prototype of ideal implementation
- positive impact of asynchronous execution
- promising comparison to other approaches
- {{{box(red, white, TODO)}}} proper out-of-core, fully-featured solvers

** Final words

#+BEGIN_EXPORT LaTeX
\begin{center}
{\Large Thank you for attending!}
\end{center}

\vfill
#+END_EXPORT

*** Acknowledgements
:PROPERTIES:
:BEAMER_env: block
:END:

- Jérôme Robert, Airbus
- Projet Région Nouvelle-Aquitaine 2018-1R50119 {{{crlf}}}
  « HPC scalable ecosystem »
- PlaFRIM experimental testbed cite:plafrim {{{smallest((supported by)}}}
  {{{smallest(Inria\, CNRS - LABRI and IMB\, Université de Bordeaux\,)}}}
  {{{smallest(Bordeaux INP and Conseil Régional d’Aquitaine))}}}
- TGCC experimental testbed cite:tgcc

** References
:PROPERTIES:
:BEAMER_OPT: fragile,allowframebreaks,label=
:END:
  
bibliographystyle:siam
bibliography:./bibliography/thesis.bib
