(require 'org) ;; Org mode support
(require 'ox-publish) ;; publishing functions
(require 'ox-latex) ;; LaTeX publishing functions
(require 'org-ref) ;; bibliography support

;; Load presets.
(load-file "compose-publish/latex-publish.el")

;; Force publishing of unchanged files to make sure all the pages get published.
;; Otherwise, the files considered unmodified based on Org timestamps are not
;; published even if they were previously deleted from the publishing directory.
(setq org-publish-use-timestamps-flag nil)

;; Load additional LaTeX packages.
(add-to-list 'org-latex-packages-alist '("T1" "fontenc"))
(add-to-list 'org-latex-packages-alist '("font = tiny" "caption"))
(add-to-list 'org-latex-packages-alist
             '("inkscapelatex = false, inkscapearea = page" "svg"))
(add-to-list 'org-latex-packages-alist '("" "xcolor"))
(add-to-list 'org-latex-packages-alist '("" "mathtools"))
(add-to-list 'org-latex-packages-alist '("" "tikz"))
(add-to-list 'org-latex-packages-alist '("" "tikzsymbols"))
(add-to-list 'org-latex-packages-alist '("" "kbordermatrix"))

;; Configure HTML website and PDF document publishing.
(setq org-publish-project-alist
      (list
       (list "solharis-2023"
             :base-directory "."
             :base-extension "org"
             :exclude ".*"
             :include ["solharis-2023.org"]
             :publishing-function '(org-beamer-publish-to-pdf-verbose-on-error)
             :publishing-directory "./public")))

(provide 'publish)
