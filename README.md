# Towards a task-based coupled direct solver for sparse/dense FEM/BEM linear systems

[![pipeline status](https://gitlab.inria.fr/thesis-mfelsoci/slides/solharis-2023/badges/master/pipeline.svg)](https://gitlab.inria.fr/thesis-mfelsoci/slides/solharis/-/commits/master)

[Slides for the 2023 plenary meeting of the SOLHARIS and the HPC scalable ecosystem projects](https://thesis-mfelsoci.gitlabpages.inria.fr/slides/solharis-2023/solharis-2023.pdf)
